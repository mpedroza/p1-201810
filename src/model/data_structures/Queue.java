package model.data_structures;


import java.util.Iterator;
import java.util.NoSuchElementException;



public class Queue<T extends Iterable<T>> implements IQueue<T> {
	private Node<T> first;    
	private Node<T> last;     
	private int n;               


	private static class Node<T> {
		private T item;
		private Node<T> next;
	}


	public Queue() {
		first = null;
		last  = null;
		n = 0;
	}


	public boolean isEmpty() {
		return first == null;
	}


	public int size() {
		return n;
	}


	public void enqueue(T item) {

		Node<T> oldlast = last;
		last = new Node<T>();
		last.item = item;
		last.next = null;

		if (isEmpty()) 
		{
			first = last;
		}
		else           
			oldlast.next = last;

		n++;
	}


	public T dequeue() {

		if (isEmpty()) 
		{
			throw new NoSuchElementException("Queue underflow");
		}
		T item = first.item;
		first = first.next;
		n--;

		if (isEmpty()) 
		{
			last = null;   
		}

		return item;
	}

}